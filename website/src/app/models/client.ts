export class Client {
    id: string = ""
    nombre: string = ""
    apellidos: string = ""
    telefono: string = ""
    correo: string = ""
    nit: string = ""
}