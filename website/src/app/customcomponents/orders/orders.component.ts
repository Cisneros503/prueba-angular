import { Component, Input, OnInit } from '@angular/core';
import { Client } from "src/app/models/client";
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';
import { Order } from "src/app/models/order";
import Swal from 'sweetalert2';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})


////*COMPONENTE CREADO PARA MOSTRAR HERENCIA
export class OrdersComponent implements OnInit {

  @Input () selectedClient: Client; 

  ordersData: any = [];
  ordersDataSelected: any = [];
  cols: any[];

  productsData: any = [];
  productsDataSelected: any = []

  constructor(
    public _order: OrderService,
    public _product: ProductService
  ) { }

  ngOnInit(): void {
    this.getProducts();
      this.getOrders();
      this.cols = [
        { field: 'id', header: 'ID' },
        { field: 'nombre', header: 'NOMBRE' },
        { field: 'descripcion', header: 'DESCRIPCION' },
        { field: 'precio', header: 'PRECIO' },

      ];
  }

  getProducts(id=-1){
    this._product.getProducts(id).subscribe(products =>{
        
        this.productsData = products;
        
    })
  }

  getOrders(id=-1){
    this._order.getOrders(id).subscribe(orders =>{
        
        this.ordersData = orders;
        
    })
  }

  createOrders(){
      if (this.productsDataSelected.length>0) {
          this.productsDataSelected.forEach(product => {
            let newOrder: Order = new Order();
            newOrder.idCliente = this.selectedClient.id;
            newOrder.fecha = new Date().toDateString();
            newOrder.id = this.GetRandom(50000).toString();
            newOrder.cantidad = "1";
            newOrder.idProducto = product.id.toString()
            this._order.addOrder(newOrder).subscribe(savedOrder=>{
                
                Swal.fire(
                  'Exito',
                  'Los datos se han guardado correctamente',
                  'success'
                )
            })
          });
      } else {
          Swal.fire(
            'Nota',
            'Tiene que seleccionar productos para crear ordenes',
            'warning'
          )
      }
      
  }

  //Genera aleatoriamente un id para guardar las ordenes 
  GetRandom(max){
    return Math.floor(Math.random() * Math.floor(max))
  }
  
  

}
