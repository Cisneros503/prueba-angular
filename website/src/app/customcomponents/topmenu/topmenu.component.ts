import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model'
import { Router } from '@angular/router';


@Component({
  selector: 'app-topmenu',
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.scss']
})

export class TopmenuComponent implements OnInit {

  faBars = faBars;
  items: MenuItem[];
  items2: MenuItem[];

  constructor(
      public topmenu: Topmenu,
      private router: Router,
      public topmenumodel: Topmenu
  ) { }

  

  ngOnInit() {
      this.topmenumodel.isVisibleSideBar = true;
      this.items = [
          {
              label: ' Mi Tienda',
              icon: 'pi pi-pw pi-home',
              command: () => {
                this.router.navigateByUrl('/mi-tienda')
              }
          },
          {
              label: 'Punto # 1',
              // icon: 'pi pi-fw pi-pencil',
              items: [
                  {label: 'Listados', 
                  icon: 'pi pi-pi pi-id-card',
                  command: () => {
                    this.router.navigateByUrl('/firstpage')
                  }
                  }
              ]
          },
          {
            label: 'Paginas',
            // icon: 'pi pi-fw pi-pencil',
            items: [
                {label: 'Clientes', 
                icon: 'pi pi-pw pi-users',
                    command: () => {
                        this.router.navigateByUrl('/mi-tienda/clientes')
                    }
                },
                {label: 'Productos', 
                icon: 'pi pi-pw pi-apple',
                    command: () => {
                        this.router.navigateByUrl('/mi-tienda/productos')
                    }
                },
                {label: 'Ordenes', 
                icon: 'pi pi-pw pi-file',
                    command: () => {
                        this.router.navigateByUrl('/mi-tienda/ordenes')
                    }
                }
                
            ],
            
        },
          
          
      ];





      this.items2 = [{
        label: 'File',
        // icon: 'pi pi-fw pi-tags',
        items: [
            {label: 'New', icon: 'pi pi-fw pi-plus'},
            {label: 'Download', icon: 'pi pi-fw pi-download'}
        ]
      },
      {
          label: 'Edit',
          items: [
              {label: 'Add User', icon: 'pi pi-fw pi-user-plus'},
              {label: 'Remove User', icon: 'pi pi-fw pi-user-minus'}
          ]
      }];




  }

  openMenu(){
      this.topmenu.isVisibleSideBar = this.topmenu.isVisibleSideBar?false: true;
  }

}
