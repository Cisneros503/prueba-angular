import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mipipe'
})
export class MipipePipe implements PipeTransform {

  ///Implementando un pipe custom simple
  transform(value: string): string {
    return value + " => Esto ha sido agregado desde un pipe personalizado...";
  }

}
