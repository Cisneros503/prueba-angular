import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMidirectiva]'
})
export class MidirectivaDirective {

  //Implementando una directiva simple (cambia el color al elemento)
  constructor(private el: ElementRef) {
      el.nativeElement.style.color="green"
  }

}
