import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { AppComponent } from 'src/app/app.component'
import { AuthGuard } from 'src/app/misc/guards/auth.guard' 
import { FirstpartComponent } from './pages/firstpart/firstpart.component';
import { MitiendaComponent } from './pages/home/mitienda/mitienda.component';
import { ClientlistComponent } from './pages/home/clientlist/clientlist.component';
import { ClientaddComponent } from './pages/clientadd/clientadd.component';
import { ProductlistComponent } from './pages/productlist/productlist.component';
import { ProductformComponent } from './pages/productform/productform.component';
import { OrderlistComponent } from './pages/orderlist/orderlist.component';
import { OrderformComponent } from './pages/orderform/orderform.component';

const routes: Routes = [
  { 
    path: 'mi-tienda', 
    component: MitiendaComponent,
    //canActivate: [AuthGuard],
  },
  { 
    path: 'mi-tienda/clientes', 
    component: ClientlistComponent 
  },
  { 
    path: 'mi-tienda/clientes/agregar', 
    component: ClientaddComponent 
  },
  { 
    path: 'mi-tienda/clientes/mostrar/:id', 
    component: ClientaddComponent 
  },
  { 
    path: 'mi-tienda/productos', 
    component: ProductlistComponent 
  },
  { 
    path: 'mi-tienda/productos/agregar', 
    component: ProductformComponent 
  },
  { 
    path: 'mi-tienda/productos/mostrar/1', 
    component: ProductformComponent 
  },
  { 
    path: 'mi-tienda/ordenes', 
    component: OrderlistComponent 
  },
  { 
    path: 'mi-tienda/crear-orden', 
    component: OrderformComponent 
  },
  { 
    path: 'firstpage', 
    component: FirstpartComponent 
  },
  { path: '**', 
    component: MitiendaComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
