import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { ButtonModule } from 'primeng/button';
import { AccordionModule } from "primeng/accordion";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { CalendarModule } from "primeng/calendar";
import { CardModule } from "primeng/card";
import { CarouselModule } from "primeng/carousel";
import { ChartModule } from "primeng/chart";
import { CheckboxModule } from "primeng/checkbox";
import { ChipsModule } from "primeng/chips";
import { CodeHighlighterModule } from "primeng/codehighlighter";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ColorPickerModule } from "primeng/colorpicker";
import { ContextMenuModule } from "primeng/contextmenu";
import { DataViewModule } from "primeng/dataview";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { FieldsetModule } from "primeng/fieldset";
import { FileUploadModule } from "primeng/fileupload";
import { GalleriaModule } from "primeng/galleria";
//import { GrowlModule } from "primeng/growl";
//import { EditorModule } from "primeng/editor";

import { InplaceModule } from "primeng/inplace";
import { InputMaskModule } from "primeng/inputmask";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { LightboxModule } from "primeng/lightbox";
import { ListboxModule } from "primeng/listbox";
import { MegaMenuModule } from "primeng/megamenu";
import { MenuModule } from "primeng/menu";
import { MenubarModule } from "primeng/menubar";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { MultiSelectModule } from "primeng/multiselect";
import { OrderListModule } from "primeng/orderlist";
import { OrganizationChartModule } from "primeng/organizationchart";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { PaginatorModule } from "primeng/paginator";
import { PanelModule } from "primeng/panel";
import { PanelMenuModule } from "primeng/panelmenu";
import { PasswordModule } from "primeng/password";
import { PickListModule } from "primeng/picklist";
import { ProgressBarModule } from "primeng/progressbar";
import { RadioButtonModule } from "primeng/radiobutton";
import { RatingModule } from "primeng/rating";
//import { ScheduleModule } from "primeng/schedule";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { SelectButtonModule } from "primeng/selectbutton";
import { SlideMenuModule } from "primeng/slidemenu";
import { SliderModule } from "primeng/slider";
import { SpinnerModule } from "primeng/spinner";
import { SplitButtonModule } from "primeng/splitbutton";
import { StepsModule } from "primeng/steps";
import { TabMenuModule } from "primeng/tabmenu";
import { TableModule } from "primeng/table";
import { TabViewModule } from "primeng/tabview";
import { TerminalModule } from "primeng/terminal";
import { TieredMenuModule } from "primeng/tieredmenu";
import { ToastModule } from "primeng/toast";
import { ToggleButtonModule } from "primeng/togglebutton";
import { ToolbarModule } from "primeng/toolbar";
import { TooltipModule } from "primeng/tooltip";
import { TreeModule } from "primeng/tree";
import { TreeTableModule } from "primeng/treetable";
import { KeyFilterModule } from "primeng/keyfilter";
import { Calendar } from 'primeng/calendar';
import {SidebarModule} from 'primeng/sidebar';

//import { DynamicDialogModule } from "primeng/components/dynamicdialog/dynamicdialog";

import {HttpClientModule} from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TopmenuComponent } from 'src/app/customcomponents/topmenu/topmenu.component';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model'
import { Client } from "./models/client";
import { Product } from "./models/product";



import { LoginComponent } from 'src/app/pages/login/login.component';
import { UserService } from 'src/app/services/user.service';
import { FirstpartComponent } from './pages/firstpart/firstpart.component';
import { ClientService } from './services/client.service';
import { ProductService } from './services/product.service';
import { OrdersComponent } from './customcomponents/orders/orders.component';
import { OrderService } from './services/order.service';
import { MitiendaComponent } from './pages/home/mitienda/mitienda.component';
import { ClientlistComponent } from './pages/home/clientlist/clientlist.component';
import { ClientaddComponent } from './pages/clientadd/clientadd.component';
import { ProductlistComponent } from './pages/productlist/productlist.component';
import { ProductformComponent } from './pages/productform/productform.component';
import { OrderlistComponent } from './pages/orderlist/orderlist.component';
import { OrderformComponent } from './pages/orderform/orderform.component';
import { MipipePipe } from './custompipes/mipipe.pipe';
import { MidirectivaDirective } from './customdirectives/midirectiva.directive';






@NgModule({
  declarations: [
    AppComponent,
    TopmenuComponent,
    LoginComponent,
    FirstpartComponent,
    OrdersComponent,
    MitiendaComponent,
    ClientlistComponent,
    ClientaddComponent,
    ProductlistComponent,
    ProductformComponent,
    OrderlistComponent,
    OrderformComponent,
    MipipePipe,
    MidirectivaDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    
    ButtonModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    GalleriaModule,
    InplaceModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessagesModule,
    MessageModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    ScrollPanelModule,
    SelectButtonModule,
    SlideMenuModule,
    SliderModule,
    SpinnerModule,
    SplitButtonModule,
    StepsModule,
    TabMenuModule,
    TableModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    KeyFilterModule,
    SidebarModule,
    //Calendar,

    FontAwesomeModule
    
    
  ],
  providers: [
    OrderService,
    ProductService,
    ClientService,
    UserService,
    Topmenu,
    Client,
    Product,
    HttpClientModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
