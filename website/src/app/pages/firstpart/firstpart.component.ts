import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ClientService } from 'src/app/services/client.service';
import { ProductService } from 'src/app/services/product.service';
import { OrdersComponent } from "src/app/customcomponents/orders/orders.component";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-firstpart',
  templateUrl: './firstpart.component.html',
  styleUrls: ['./firstpart.component.scss']
})


export class FirstpartComponent implements OnInit {

  @ViewChild(OrdersComponent, { static: false }) ordersComponent: OrdersComponent;

  indexList: number = 0; //Determina cual es el listado seleccionado (0=Clientes / 1=Productos / 2=Ordenes)
  cols: any[];
  tableData: any;
  selectedClient: any = [];

  nitRegex: RegExp = /[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$/;
  

  clientForm = new FormGroup({
      id: new FormControl('', Validators.required),
      nombre: new FormControl(''),
      apellidos: new FormControl(''),
      telefono: new FormControl(''),
      correo: new FormControl('',[Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
      nit: new FormControl('')
  });

  productForm = new FormGroup({
        id: new FormControl('', Validators.required),
        nombre: new FormControl(''),
        descripcion: new FormControl(''),
        precio: new FormControl('')
  });

  constructor(
    public topmenu: Topmenu,
    public _client: ClientService,
    public _product: ProductService
  ) { }

  ngOnInit(): void {
      this.getClients();
      this.initTableHeaders(this.indexList);
  }

  //Inicializa la data de la tabla dependiendo de el tipo seleccionado
  initTableHeaders(indexList){
    switch (indexList) {
      case 0: // Clientes
        this.cols = [
          { field: 'id', header: 'ID' },
          { field: 'nombre', header: 'NOMBRE' },
          { field: 'apellidos', header: 'APELLIDOS' },
          { field: 'telefono', header: 'TELEFONO' },
          { field: 'correo', header: 'CORREO' },
          { field: 'nit', header: 'NIT' }
        ];
        this.getClients();
        break;
      case 1: // Productos
          this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'nombre', header: 'NOMBRE' },
            { field: 'descripcion', header: 'DESCRIPCION' },
            { field: 'precio', header: 'PRECIO' },
          ];
          this.getProducts()
          break;
      case 2: // Ordenes

          this.getClients()
          break;
    
      default:
        break;
    }
    
  }

  //Detecta los cambios de listados (cliente, producto y orden)
  handleChange(e){
     
      this.initTableHeaders(e.index)
  }

  getClients(id=-1){
      this._client.getClients(id).subscribe(clients =>{
          this.clientForm = new FormGroup({
            id: new FormControl('', Validators.required),
            nombre: new FormControl(''),
            apellidos: new FormControl(''),
            telefono: new FormControl(''),
            correo: new FormControl('',[Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
            nit: new FormControl('')
          });
          this.tableData = clients;
          
      })
  }

  createClient(){

      let idClientExist = this.tableData.find(cl=> cl.id == this.clientForm.controls.id.value)
      if(idClientExist){
          Swal.fire(
            'Nota',
            'El id Ingresado ya está asignado por favor ingrese un id válido',
            'warning'
          )
      }else{
          this._client.addClient(this.clientForm.value).subscribe(savedClient=>{
          
          this.getClients(-1);
      })
      }
     
  }

  getProducts(id=-1){
    this._product.getProducts(id).subscribe(products =>{
        this.productForm = new FormGroup({
          id: new FormControl('', Validators.required),
          nombre: new FormControl(''),
          descripcion: new FormControl(''),
          precio: new FormControl(''),
        });
        this.tableData = products;
       
        
    })
  }

  createProduct(){
      
      this._product.addProduct(this.productForm.value).subscribe(savedClient=>{
          
      })
  }

  clientSelectedChange(){
      console.log(this.selectedClient);
    
  }

  createOrders(){
      
      this.ordersComponent.createOrders();
  }

  

}
