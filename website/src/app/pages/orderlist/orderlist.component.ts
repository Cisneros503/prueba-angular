import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ClientService } from 'src/app/services/client.service';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';
import { Order } from './../../models/order';


@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.scss']
})
export class OrderlistComponent implements OnInit {

  cols: any[];
  tableData: any = [];


  constructor(
    private _order: OrderService,
    private _router: Router,
    public topmenu: Topmenu,
    public _product: ProductService,
    public _client: ClientService,
  ) { }

  ngOnInit(): void {
      this.cols = [
        { field: 'id', header: 'ID' },
        { field: 'Producto', header: 'PRODUCTO' },
        { field: 'Cliente', header: 'CLIENTE' },
        { field: 'cantidad', header: 'CANTIDAD' },
        { field: 'fecha', header: 'FECHA' },
      ];
      this.getOrders();
  }

  //Obtiene la orden y transforma la data para bindearla en la tabla
  getOrders(id=-1){
    this._order.getOrders().subscribe(orders =>{

        this._product.getProducts().subscribe(products =>{

            this._client.getClients().subscribe(clients =>{

              orders.forEach((order, index) => {
                  let orderFormat: any = [];
                  orderFormat.id = order.id;
                  let prod = products.find(pr=> pr.id == order.idProducto);
                  orderFormat.Producto = prod.nombre;
                  let cli = clients.find(cl=> cl.id == order.idCliente);
                  orderFormat.Cliente = cli.nombre
                  orderFormat.cantidad = order.cantidad;
                  orderFormat.fecha = order.fecha;

                  this.tableData.push(orderFormat);
                  
              });

            })
        
        })
        
    })
  }

  goToAddOrders(){
      this._router.navigate(["mi-tienda/crear-orden"]);
  }

}
