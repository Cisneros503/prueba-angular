import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ClientService } from 'src/app/services/client.service';
import Swal from 'sweetalert2';
import { ClientinfoService } from './clientinfo.service';

@Component({
  selector: 'app-clientadd',
  templateUrl: './clientadd.component.html',
  styleUrls: ['./clientadd.component.scss']
})
export class ClientaddComponent implements OnInit {

  Clients:any [];
  tittle: string
  isDetail: boolean = false

  clientForm = new FormGroup({
      id: new FormControl('', Validators.required),
      nombre: new FormControl(''),
      apellidos: new FormControl(''),
      telefono: new FormControl(''),
      correo: new FormControl('',[Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
      nit: new FormControl('')
  });

  constructor(
      private _activatedRouter: ActivatedRoute, 
      public topmenu: Topmenu,
      public _client: ClientService,
      public _clientInfo: ClientinfoService
  ) { }

  ngOnInit(): void {
      let client = this._clientInfo.getClient()
     // let id = this._activatedRouter.snapshot.params['id']
     
      this._clientInfo.unsetClient();
      if(client.id.length>0){
          this.isDetail = true;
          this.tittle = "Pagina: Detalle de Cliente";
          this.setClientForm(client.id);
      }else{
          this.tittle = "Pagina: Agregar Cliente";
      }
      
      this._client.getClients().subscribe(clients =>{
          this.Clients = clients;
      })
  }

  createClient(){
      let idClientExist = this.Clients.find(cl=> cl.id == this.clientForm.controls.id.value)
      if(idClientExist){
          Swal.fire(
            'Nota',
            'El id Ingresado ya está asignado por favor ingrese un id válido',
            'warning'
          )
      }else{
          this._client.addClient(this.clientForm.value).subscribe(savedClient=>{
              
              this._client.getClients().subscribe(clients =>{
                this.Clients = clients;
              })
              Swal.fire(
                'Exito!',
                'El registro se ha guardado',
                'success'
              )
          })
      }
 }

  setClientForm(id){
        this._client.getClients(id).subscribe(client =>{
        
            this.clientForm.patchValue(client)
            console.log("valor asignado al formulario de cliente");
            console.log(this.clientForm.value);
            
            
        })
  }

}
