import { Injectable } from '@angular/core';
import { Client } from './../../models/client';


@Injectable({
  providedIn: 'root'
})
export class ClientinfoService {

  public client: Client = new Client();

  constructor() { }

  getClient(){
    return this.client;
  }

  setClient(clientObject){
    this.client = clientObject;
  }

  unsetClient(){
    this.client = new Client();
  }



}
