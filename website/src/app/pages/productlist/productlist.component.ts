import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ProductService } from 'src/app/services/product.service';
import { ProductInfoService } from '../productform/product-info.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {

  cols: any[];
  tableData: any;

  constructor(
      private _productInfo: ProductInfoService,
      private _router: Router,
      public topmenu: Topmenu,
      public _product: ProductService
  ) { }

  ngOnInit(): void {
      this.cols = [
        { field: 'id', header: 'ID' },
        { field: 'nombre', header: 'NOMBRE' },
        { field: 'descripcion', header: 'DESCRIPCION' },
        { field: 'precio', header: 'PRECIO' },
      ];
      this.getProducts();
  }

  getProducts(id=-1){
    this._product.getProducts(id).subscribe(products =>{
        this.tableData = products;
        
    })
  }

  viewProduct(product){
    this._productInfo.setProduct(product)
    this._router.navigate(["mi-tienda/productos/mostrar/1",]);
  }

  goToAddProducts(){
      this._router.navigate(["mi-tienda/productos/agregar"]);
  }

}
