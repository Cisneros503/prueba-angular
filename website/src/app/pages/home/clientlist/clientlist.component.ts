import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ClientService } from 'src/app/services/client.service';
import { ClientinfoService } from "src/app/pages/clientadd/clientinfo.service";

@Component({
  selector: 'app-clientlist',
  templateUrl: './clientlist.component.html',
  styleUrls: ['./clientlist.component.scss']
})
export class ClientlistComponent implements OnInit {

  tableData: any;
  cols: any[];

  constructor(
    private _clientInfo: ClientinfoService,
    private _router: Router,
    public topmenu: Topmenu,
    public _client: ClientService,
  ) { }

  ngOnInit(): void {
    this.getClients();
    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'nombre', header: 'NOMBRE' },
      { field: 'apellidos', header: 'APELLIDOS' },
      { field: 'telefono', header: 'TELEFONO' },
      { field: 'correo', header: 'CORREO' },
      { field: 'nit', header: 'NIT' }
    ];
  }

  getClients(id=-1){
    this._client.getClients(id).subscribe(clients =>{
        this.tableData = clients;
        
    })
  }

  viewClient(client){
      this._clientInfo.setClient(client)
      this._router.navigate(["mi-tienda/clientes/mostrar",client.id]);
  }

  goToAddClients(){
      this._router.navigate(["mi-tienda/clientes/agregar"]);
  }

}
