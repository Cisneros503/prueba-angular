import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product';


@Injectable({
  providedIn: 'root'
})
export class ProductInfoService {

  product: Product = new Product();

  constructor() { }


  getProduct(){
    return this.product;
  }

  setProduct(productObject){
    this.product = productObject;
  }

  unsetProduct(){
    this.product = new Product();
  }


}
