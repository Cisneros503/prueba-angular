import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';
import { ProductInfoService } from './product-info.service';

@Component({
  selector: 'app-productform',
  templateUrl: './productform.component.html',
  styleUrls: ['./productform.component.scss']
})
export class ProductformComponent implements OnInit {

  Products:any [];
  tittle: string
  isDetail: boolean = false

  productForm = new FormGroup({
      id: new FormControl('', Validators.required),
      nombre: new FormControl(''),
      descripcion: new FormControl(''),
      precio: new FormControl('')
  });

  constructor(
      public topmenu: Topmenu,
      public _product: ProductService,
      private _productInfo: ProductInfoService,
  ) { }

  ngOnInit(): void {
      let product = this._productInfo.getProduct()
      // let id = this._activatedRouter.snapshot.params['id']
      this._productInfo.unsetProduct();
      if(product.id.length>0){
          this.isDetail = true;
          this.tittle = "Pagina: Detalle de Producto";
          this.setProductForm(product.id);
      }else{
          this.tittle = "Pagina: Agregar Producto";
      }
      
      this._product.getProducts().subscribe(products =>{
          this.Products = products;
      })
  }

  setProductForm(id){
    this._product.getProducts(id).subscribe(product =>{

        this.productForm.patchValue(product)
        console.log("valor asignado al formulario de producto");
        console.log(this.productForm.value);
    })
  }

  createProduct(){
    let idProductExist = this.Products.find(cl=> cl.id == this.productForm.controls.id.value)
    if(idProductExist){
        Swal.fire(
          'Nota',
          'El id Ingresado ya está asignado por favor ingrese un id válido',
          'warning'
        )
    }else{
        this._product.addProduct(this.productForm.value).subscribe(savedProduct=>{
            this._product.getProducts().subscribe(products =>{
              this.Products = products;
            })
            Swal.fire(
              'Exito!',
              'El registro se ha guardado',
              'success'
            )
        })
    }
}

  

}
