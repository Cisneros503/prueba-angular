import { FormGroup, FormControl,FormArray, FormBuilder } from '@angular/forms'
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { ClientService } from 'src/app/services/client.service';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orderform',
  templateUrl: './orderform.component.html',
  styleUrls: ['./orderform.component.scss']
})
export class OrderformComponent implements OnInit {

  Orders: any = [];
  Products: any = [];
  selectedProd = null;
  Clients: any = [];
  selectedCli = null;
  qty = 1
 

  orderObj: any = {
    idProducto: -1,
    idCliente: -1,
    id: null,
    cantidad: null,
    fecha: null
  }
  

  constructor(
      private _order: OrderService,
      private _router: Router,
      public topmenu: Topmenu,
      public _product: ProductService,
      public _client: ClientService,
  ) { }


  ngOnInit(): void {

      this._client.getClients().subscribe(clients =>{
          this.Clients = clients
      })
      this._product.getProducts().subscribe(products =>{   
          this.Products = products
      })
      this._order.getOrders().subscribe(orders =>{
          this.Orders = orders
      })

  }

  createOrder(){

    if (
        !this.selectedProd || !this.selectedCli || !this.orderObj.id || !this.orderObj.fecha
        || this.orderObj.id.length==0 || this.qty<1 || this.orderObj.fecha.length==0
    ) {
        Swal.fire(
          'Aviso!',
          'Todos los campos son obligatorios',
          'warning'
        )
    } else {

        let idOrderExist = this.Orders.find(ord=> ord.id == this.orderObj.id)
        if(idOrderExist){
            Swal.fire(
              'Nota',
              'El id Ingresado ya está asignado por favor ingrese un id válido',
              'warning'
            )
        }else{
            this.orderObj.cantidad = this.qty.toString();
            this.orderObj.idProducto =  this.selectedProd.id; 
            this.orderObj.idCliente = this.selectedCli.id;
    
            this._order.addOrder(this.orderObj).subscribe(savedOrder=>{
                
                Swal.fire(
                  'Exito',
                  'Los datos se han guardado correctamente',
                  'success'
                )
                this._router.navigate(["mi-tienda/ordenes"]);
            })
        }

        
    }

    
}

}
