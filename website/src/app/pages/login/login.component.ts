import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Password } from 'primeng/password/password';
import { FormsModule, FormGroup, ReactiveFormsModule, FormControl, Validators } from '@angular/forms';
import { Topmenu } from 'src/app/customcomponents/topmenu/topmenu.model';
import { UserService } from 'src/app/services/user.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm = new FormGroup({
    name: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  cars: any[];
  selectedCars: any[];
  data: any;

  cols:any

  constructor(
    public topmenu: Topmenu,
     private _userService: UserService,
  ) { }

  ngOnInit() {
    Swal.fire({
      text: 'Hello!',
      icon: 'success'
    });

    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' }
    ];

    this.data = [
      {vin: 'Unqualified', year: 'unqualified'},
      {vin: 'Qualified', year: 'qualified'},
      {vin: 'New', year: 'new'},
      {vin: 'Negotiation', year: 'negotiation'},
      {vin: 'Renewal', year: 'renewal'},
      {vin: 'Proposal', year: 'proposal'}
    ]


  }

  authenticate(){
      this._userService.authenticate(this.loginForm.value).subscribe((data: any)=>{
        
        let x = data.ad
      },(er)=>{

      },()=>{

      })
  }

  opensweetalertcst(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
      Swal.fire(
        'Deleted!',
        'Your imaginary file has been deleted.',
        'success'
      )
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
        'Your imaginary file is safe :)',
        'error'
      )
      }
    })
  }

  submit(){
   // console.log(this.loginForm)
  }

}
