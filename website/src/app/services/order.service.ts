import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Order } from './../models/order';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})

export class OrderService {

  httpHeaders: any;

  constructor(
    private _http: HttpClient,
    private config: ConfigService,
  ) { }

  getOrders(id:any = -1){
    id = id > 0 || id.length > 0? id.toString(): '';
    return this._http.get(`${this.config.urlApi}/ordenes/${id}`, {
        headers: this.httpHeaders,
    }).pipe(
        map((resp: any) => {
            console.log('Respuesta de la api... Metodo(ordenes) ');
            console.log(resp)
            return resp;
        }),
        catchError(err => {
            console.log(err)
            return err;
            
        })
    )
  }

  addOrder(product: Order): Observable<any> {
    return this._http.post(`${this.config.urlApi}/ordenes`, product, {headers: this.httpHeaders })
    .pipe(
        map((resp: any) => {
            
            return resp;
        }),
        catchError(err => {
            Swal.fire(
              'Error',
              'No se ha podido agregar el regitro',
              'warning'
            )
            return Observable.throw(err);

        })
    )
  }

}
