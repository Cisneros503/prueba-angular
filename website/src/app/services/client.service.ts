import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Client } from './../models/client';
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  httpHeaders: any;

  constructor(
    private _http: HttpClient,
    private config: ConfigService,
  ) { }

  getClients(id:any = -1){
      id = id > 0 || id.length > 0? id.toString(): '';
      return this._http.get(`${this.config.urlApi}/clientes/${id}`, {
          headers: this.httpHeaders,
      }).pipe(
          map((resp: any) => {
              console.log('Respuesta de la api... Metodo(Clientes) ');
              console.log(resp)
              return resp;
          }),
          catchError(err => {
              console.log(err)
              return err;
              
          })
      )
  }

  addClient(client: Client): Observable<any> {
        return this._http.post(`${this.config.urlApi}/clientes`, client, {headers: this.httpHeaders })
        .pipe(
            map((resp: any) => {
                //*comento por que no permite ver la actualizacion de la tabla luego de que agrega el registro.
                // Swal.fire(
                //   'Nota',
                //   'El Cliente se ha agregado con exito',
                //   'success'
                // )
                return resp;
            }),
            catchError(err => {
                Swal.fire(
                  'Error',
                  'No se ha podido agregar el regitro',
                  'warning'
                )
                return Observable.throw(err);

            })
        )
    }

}
