import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

    data: any = {
        url: 'https://reqres.in'
    }

    httpHeaders = new HttpHeaders()

    constructor(
        private _http: HttpClient
    ) { }

    authenticate (userForm: any){
        console.log(userForm);
        let page = '2';
        
        const httpParams = new HttpParams()
               .set('param3', page)

        return this._http.get(this.data.url+"/api/users/", {
            headers: this.httpHeaders,
            params: httpParams
        }).pipe(
            map((resp: any) => {
                console.log(resp)
                return resp.data;
            }),
            catchError(err => {
                console.log(err)
                return err;
                
            })
        )
    }

    // FunctionGetPost(): Observable<any> {
        // return this._http.post(getBaseUrl()  + 'api/CostDetail/Update_tmp/'+idEnterprise, costDetail, {headers: this.httpHeaders })
        // .pipe(
        //     map((resp: any) => {
        //         return resp;
        //     }),
        //     catchError(err => {
        //         swal('Error.', err.error.exception.Message);
        //         return Observable.throw(err);

        //     })
        // )
    // }

    



}
