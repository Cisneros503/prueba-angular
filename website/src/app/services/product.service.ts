import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Product } from './../models/product';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  httpHeaders: any;

  constructor(
    private _http: HttpClient,
    private config: ConfigService,
  ) { }


  getProducts(id:any = -1){
      id = id > 0 || id.length > 0? id.toString(): '';
      return this._http.get(`${this.config.urlApi}/productos/${id}`, {
          headers: this.httpHeaders,
      }).pipe(
          map((resp: any) => {
              console.log('Respuesta de la api... Metodo(productos) ');
              console.log(resp)
              return resp;
          }),
          catchError(err => {
              console.log(err)
              return err;
              
          })
      )
  }

  addProduct(product: Product): Observable<any> {
    return this._http.post(`${this.config.urlApi}/productos`, product, {headers: this.httpHeaders })
    .pipe(
        map((resp: any) => {
            
            return resp;
        }),
        catchError(err => {
            Swal.fire(
              'Error',
              'No se ha podido agregar el regitro',
              'warning'
            )
            return Observable.throw(err);

        })
    )
  }

}
